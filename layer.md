# Schichtenmodell
---

## Ein Beispiel: Der Paketdienst der Post[^1]

Bevor wir die komplexe Übertragung von Nachrichten im Internet untersuchen, schauen wir uns erst einmal eine Übertragungssituation im Alltag an: Wie wird die Übertragung von Paketen durch die Post organisiert?

Wenn ein Absender der Post ein Paket zur Übertragung an einen Empfänger übergibt, dann wird bekanntlich kein Postbote mit dem Paket losgeschickt, sondern es beginnt ein durchdachter komplexer Übertragungsprozess.

Zur Kundenbetreuung hat die Post Dienststellen eingerichtet, die für die Annahme und Zustellung der Pakete zuständig sind. Sowohl die Annahme eines Paketes als auch die Zustellung eines Paketes laufen nach vorgegebenen Protokollen ab. So wird bei der Annahme eines Paketes vom Kunden eine Paketkarte ausgefüllt. Die Zustellung erfolgt in der Regel so, dass Postboten drei Zustellungsversuche machen. Trifft der Postbote den Empfänger an, dann wird das Paket dem Empfänger übergeben und die Zustellung quittiert. Ansonsten wird der Empfänger benachrichtigt und muss dann selbst das Paket in der angegebenen Dienststelle abholen.

Für die Vermittlung von Paketen hat die Post ein Logistiksystem entwickelt, das vereinfacht so funktioniert: Pakete werden (mit Briefen und Postkarten) in Postsäcke gefüllt. Postsäcke werden dann an bestimmte Zentraleinheiten übermittelt, wo sie so neu zusammengestellt werden, so dass ein optimaler Weitertransport an den Empfängerort gewährleistet wird. Alle die Vorgänge sind durch Protokolle klar geregelt.

Für den Transport von Postsäcken zwischen zwei Zentraleinheiten sind dann posteigene oder auch andere Transportunternehmen zuständig. Die Transportunternehmer nutzen je nach Transportsituation Kleintransporter, grössere LKWs, die Bahn, Schiffe oder auch Flugzeuge.

Die Übertragung von Paketen durch die Post kann also durch ein mehrschichtiges Modell beschrieben werden.

![](images/post.png)

Jede Schicht stellt der darüberliegenden Schicht bestimmte Dienste bereit. So garantiert die Transportschicht den Transport von Postsäcken zwischen zwei Diensteinheiten der Post. Die Vorgänge in der Schicht selbst werden durch Protokolle geregelt. So könnte ein Transportprotokoll festlegen, dass Postsäcke um 17:00 Uhr an einer Dienststelle abgeholt und auf dem schnellsten Weg mit einem Kleintransporter zur nächstliegenden Zentraleinheit gebracht werden.

Aufgabe 1
Der Bau eines Autos ist ebenfalls ein komplexer Vorgang. Versuche, diesen Vorgang durch ein Schichtenmodell zu strukturieren.


[^1]: Quelle: [inf-schule](https://www.inf-schule.de/kommunikation/netze/schichtenmodelle/schichtenmodelle_alltag)
