# HTML
---

## Auszeichnungssprache

Hast Du schon einmal **\*lach\*** oder etwas Ähnliches geschrieben? Dann hast Du eine Auszeichnungssprache benutzt.

Die Idee dahinter ist, dass bestimmte Teile eines Textes nicht den eigentlichen Inhalt darstellen, sondern definieren, wie der Inhalt zu verstehen ist. Im vorangehenden Beispiel bedeuten die Sternchen, dass mit *lach* eine Tätigkeit gemeint ist. Auszeichnungssprachen dienten ursprünglich als Anweisungen für die Setzer im Buchdruck. Auch HTML ist eine Auszeichnungssprache.

HTML verwendet für die Auszeichnung von bestimmten Textteilen sogenannte *Tags* (englisch). Die Textstelle wird dabei von einem Start- und einem End-*Tag* umschlossen. In HTML würde man schreiben:

``` html
<em>lach</em>
```

## *Tags* und Elemente

In HTML werden Textteile ausgezeichnet, indem sie mit *Tags* umschlossen werden. Ein Start-*Tag* besteht aus einem `<`-Zeichen, einem Namen und einem `>`-Zeichen. Bei einem End-*Tag* wird dem Namen ein Schrägstrich vorangestellt:

``` html
<q>Carpe diem!</q>
```

Start-*Tag*, Text und End-*Tag* bilden zusammen ein Element, hier ein `q`-Element. Der Text wird *Inhalt* des Elements genannt.

![](images/html-element.svg)

## Elemente verschachteln

Der Inhalt eines Elements kann wiederum Elemente enthalten. So kann eine umfangreiche Struktur von verschachtelten Elementen aufgebaut werden. Dabei muss ein inneres Element immer vollständig von den Tags des äusseren umschlossen werden:

``` html
<strong>Ein <em>korrektes</em> Beispiel.</strong>
```

Nicht gestattet sind Elemente, die sich überschneiden:

``` html
<strong>Ein <em>falsches</strong> Beispiel.</em>
```

## Attribute

Attribute werden nur im Start-Tag definiert. Attribute haben immer einen Namen und einen Wert. Zwischen Name und Wert eines Attributs steht ein Gleichheitszeichen `=`. Vor und nach dem Gleichheitszeichen wird kein Leerzeichen gesetzt. Der Werte eines Attributs steht immer in doppelten Anführungszeichen `"`.

**Beispiel:** Das `a`-Element stellt einen Link dar. Die Ziel-Webseite des Links wird im Attribut `href` (engl. *hypertext reference*) angegeben:

``` html
<a href="https://www.gymkirchenfeld.ch">Gymnasium Kirchenfeld</a>
```

::: example
<a href="https://www.gymkirchenfeld.ch">Gymnasium Kirchenfeld</a>
:::

## Elemente ohne Inhalt

Es gibt auch Elemente, welche keinen Inhalt haben können. Bei diesen Elementen muss auch das End-*Tag* weggelassen werden. Beispiele sind die Elemente für einen Zeilenumbruch `<br>` (engl. *break*) oder ein Bild `<img>` (engl. *image*).
