# Listen
---

In HTML gibt es zwei Arten von Listen, welche sich durch die verwendeten Aufzählungszeichen unterscheiden.

## Ungeordnete Listen

Ungeordnete Listen (engl. *unordered list*) beginnen mit dem *Tag* `<ul>`. Hier erhalten alle Listeneinträge das gleiche Aufzählungszeichen.

Eine Liste darf nur `<li>`-Elemente (engl. *list item*) enthalten, welche für einen Listeneintrag stehen.

``` html
<ul>
  <li>Erstens</li>
  <li>Zweitens</li>
  <li>Drittens</li>
</ul>
```

::: example
<ul>
  <li>Erstens</li>
  <li>Zweitens</li>
  <li>Drittens</li>
</ul>
:::

## Geordnete Listen

Geordnete Listen (engl. *ordered list*), welche mit dem *Tag* `<ol>` markiert werden, erhalten ein fortlaufendes Aufzählungszeichen, beispielsweise «1, 2, 3».

``` html
<ol>
  <li>Erstens</li>
  <li>Zweitens</li>
  <li>Drittens</li>
</ol>
```

::: example
<ol>
  <li>Erstens</li>
  <li>Zweitens</li>
  <li>Drittens</li>
</ol>
:::

Die Nummerierungsart kann durch folgende Attribute des `<ol>`-*Tags* gesteuert werden:

| Attribut   | Werte                   | Bedeutung                                        |
|:---------- |:----------------------- |:------------------------------------------------ |
| `reversed` | `reversed`              | Die Nummerierung erfolgt absteigend (9, 8, 7 ,…) |
| `start`    | Zahl                    | der Startwert für die Nummerierung               |
| `type`     | `1`, `A`, `a`, `I`, `i` | Die Art der Nummerierung                         |

Es gibt folgende Arten der Nummerierung:

| Typ | Bedeutung                        | Beispiel       |
|:--- |:-------------------------------- |:-------------- |
| `1` | nummerisch                       | 1, 2, 3, 4     |
| `A` | alphabetisch mit Grossbuchstaben | A, B, C, D     |
| `a` | alphabetisch mit Kleinbuchstaben | a, b, c, d     |
| `I` | römisch mit Grossbuchstaben      | I, II, III, IV |
| `i` | römisch mit Kleinbuchstaben      | i, ii, iii, iv |


``` html
<ol reversed="reversed" start="5">
  <li>Fünftens</li>
  <li>Viertens</li>
  <li>Drittens</li>
</ol>
```

::: example
<ol reversed="reversed" start="5">
  <li>Fünftens</li>
  <li>Viertens</li>
  <li>Drittens</li>
</ol>
:::


::: exercise Aufgabe 5 – Liste

Füge eine Liste in deine Webseite ein.

Überprüfe die Datei mit dem [W3C Markup Validation Service](https://validator.w3.org/#validate_by_upload).
:::
