# Farben
---

## Farbdarstellung

In CSS können Farben nach dem bekannten RGB-Schema angegeben werden:

<VueColourPicker type="rgb" output="css"/>

## Farben in CSS

Die Schriftfarbe wird durch die CSS-Eigenschaft `color` festgelegt, die Hintergrundfarbe durch `background-color`. Die folgende CSS-Regel legt fest, dass die ganze Webseite in roter Schrift auf schwarzem Hintergrund dargestellt werden soll:

``` css
body {
    color: red;
    background-color: black;
}
```

In CSS können Farben auch als RGB-Werte definiert werden:

``` css
rgb(Rotanteil, Grünanteil, Blauanteil)
```

Die drei Farbanteile können entweder als Prozentwert oder als Wert zwischen 0 und 255 angegeben werden.

``` css
body {
    color: rgb(255, 0, 150);
    background-color: rgb(100%, 80%, 0);
}
```

Eine veraltete, jedoch noch weit verbreitete Methode, RGB-Farben zu definieren, ist die hexadezimale Farbangabe:

``` css
#RRGGBB
```

Hier stehen RR, GG und BB für eine hexadezimale Darstellung der Rot- Grün- und Blauwerte in einem Bereich von 0 bis 255 (00 bis FF hexadezimal). Dabei entspricht der Wert FF einem Anteil von 100%.

``` css
body {
    background-color: #FFCC00;
}
```

::: exercise Aufgabe 7 – CSS

1. Erstelle in **Notepad++** eine neue Datei und speichere sie als **style.css**.
2. Erstelle in **style.css** eine Regel für die Hintergrundfarbe von `body`.
3. Erstelle in **index.html** eine Referenz auf die CSS-Datei.
:::
