# HTML *Hypertext Markup Language*
---

::: cards 2
#### [Grundstruktur](?page=2-structure)

``` html
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8">
    <title>Titel der Webseite</title>
  </head>
  <body>
    Inhalt der Webseite
  </body>
</html>
```

***
#### [Textstruktur](?page=3-text-structure)

| *Tag*                     | Bedeutung     |
|:------------------------- |:------------- |
| `<h1>`, `<h2>` ... `<h6>` | Überschrift   |
| `<div>`                   | Gruppierung   |
| `<p>`                     | Absatz        |
| `<br>`                    | Zeilenumbruch |

***
#### [Zeichenformatierung](?page=4-inline)

| Start-*Tag* | Bedeutung             |
|:----------- |:--------------------- |
| `<em>`      | Betonung (*emphasis*) |
| `<strong>`  | starke Betonung       |
| `<q>`       | Zitat (*quote*)       |
| `<span>`    | Gruppierung           |

***
#### [Verknüpfungen](?page=5-links)

| *Tag*            | Bedeutung        |
|:---------------- |:---------------- |
| `<img>`          | Bilddatei        |
| `<a href="...">` | Link             |
| `<link>`         | CSS-Datei        |
| `<script>`       | JavaScript-Datei |

***
#### [Listen](?page=6-list)

| *Tag*  | Bedeutung         |
|:------ |:----------------- |
| `<ol>` | geordnete Liste   |
| `<ul>` | ungeordnete Liste |
| `<li>` | Listeneintrag     |

***
#### [Tabellen](?page=7-table)

| *Tag*       | Bedeutung       |
|:----------- |:--------------- |
| `<table>`   | Gesamte Tabelle |
| `<caption>` | Beschriftung    |
| `<th>`      | Kopfzeile       |
| `<tr>`      | Tabellenzeile   |
| `<td>`      | Zelle           |

:::
