# Box-Modell
---

Im Browser werden HTML-Block-Elemente mit Hilfe von verschachtelten Rechtecken dargestellt:

- dem Inhaltsbereich,
- einem **Innenabstand** (padding),
- einem **Rahmen** (border) und
- einem **Aussenabstand** (margin).

Diese Anordnung wird «Box-Modell» (engl. *box model*) genannt.

![Illustration des Box-Modells](images/box-model.svg)

Wenn der Hintergrund eines Elements festgelegt wird, bezieht sich dies immer auf Inhalt sowie Innenabstand.


## Inhaltsbereich

Die Grösse des Inhaltsbereichs wird normalerweise aufgrund des Inhalts automatisch vom Browser berechnet. Sie kann jedoch auch mit den CSS-Eigenschaften `width` und `height` festgelegt werden.

Die folgende CSS-Regel legt die Grösse des Bildes mit der ID `bird` fest:

``` css
img#bird {
  width: 5cm;
  height: 3cm;
}
```

## Innenabstand

Der Innenabstand wird mit den folgenden CSS-Eigenschaften festgelegt:

| Eigenschaft      | Bedeutung                |
|:---------------- |:------------------------ |
| `padding`        | Innenabstand alle Seiten |
| `padding-bottom` | Innenabstand unten       |
| `padding-left`   | Innenabstand links       |
| `padding-right`  | Innenabstand rechts      |
| `padding-top`    | Innenabstand oben        |

Dabei legt `padding` den Abstand auf allen Seiten gleichzeitig fest, die anderen Eigenschaften legen den Abstand für die entsprechende Seite fest.

Diese Eigenschaften können auch kombiniert verwendet werden:

``` css
div {
  padding: 1cm; /* alle Seiten 1cm */
  padding-bottom: 2cm; /* aber unten 2cm */
}
```

## Rahmen

Der Rahmen wird durch diese CSS-Eigenschaften definiert:

| Eigenschaft     | Bedeutung          |
|:--------------- |:------------------ |
| `border`        | Rahmen alle Seiten |
| `border-bottom` | Rahmen unten       |
| `border-left`   | Rahmen links       |
| `border-right`  | Rahmen rechts      |
| `border-top`    | Rahmen oben        |

Als Wert für diese Eigenschaften muss immer eine Kombination aus **Rahmenbreite**, **Rahmentyp** und **Farbe** angegeben werden:

``` css
figure {
  border: 20px solid red;
}
```

Für den Rahmentyp sind folgende Werte möglich:

| Wert     | Bedeutung           |
|:-------- |:------------------- |
| `none`   | kein Rahmen         |
| `dotted` | gepunktete Linie    |
| `dashed` | gestrichelte Linie  |
| `solid`  | durchgezogene Linie |
| `double` | Doppellinie         |
| `groove` | 3D-Effekt           |
| `ridge`  | 3D-Effekt           |
| `inset`  | 3D-Effekt           |
| `outset` | 3D-Effekt           |

## Aussenabstand

Der Aussenabstand wird mit den folgenden CSS-Eigenschaften festgelegt:

| Eigenschaft     | Bedeutung                 |
|:--------------- |:------------------------- |
| `margin`        | Aussenabstand alle Seiten |
| `margin-bottom` | Aussenabstand unten       |
| `margin-left`   | Aussenabstand links       |
| `margin-right`  | Aussenabstand rechts      |
| `margin-top`    | Aussenabstand oben        |

Dabei legt `margin` den Abstand auf allen Seiten gleichzeitig fest, die anderen Eigenschaften legen den Abstand für die entsprechende Seite fest.

Diese Eigenschaften können auch kombiniert verwendet werden:

``` css
div {
  margin: 1cm; /* alle Seiten 1cm */
  margin-bottom: 2cm; /* aber unten 2cm */
}
```

## Element zentrieren

Mit Hilfe des Box-Modells lassen sich Inhalte praktisch beliebig auf der Webseite platzieren. Um den Inhalt einer Webseite zentriert im Browser darzustellen, kann folgende CSS-Definition verwendet werden:

``` css
main {
  margin-left: auto;
  margin-right: auto;
  width: 50%
}
```

::: exercise Aufgabe 10 – Zentrieren
Platziere den Inhalt deiner Webseite zentriert im Browser.
:::
