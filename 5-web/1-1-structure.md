# Grundstruktur
---

Die Grundstruktur eine HTML-Datei sieht so aus:

``` html
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8">
    <title>Titel der Webseite</title>
  </head>
  <body>
    Inhalt der Webseite
  </body>
</html>
```

Jede HTML-Datei muss mit dem speziellen *Tag* `<!DOCTYPE html>` beginnen. Daran kann ein Browser erkennen, dass es sich um eine HTML-Datei handelt. Auf die Dateiendung kann sich der Browser nicht verlassen, bei dynamisch erzeugten HTML-Dateien lautet die Dateiendung anders.

Anschliessend folgt ein `<html>`-*Tag*, in welchem mit dem Attribut `lang="de"` die Sprache, in welcher die Webseite verfasst ist, angegeben wird. Das schliessende `</html>`-*Tag* steht an letzter Stelle in der Datei.

Innerhalb der `<html>`-*Tags* stehen der **Kopf** (*head*) und der **Körper** (*body*) der Webseite. Der Kopf ist von `<head>`-*Tags* umgeben und enthält Informationen *über* die Webseite. Im Körper steht der eigentliche Inhalt der Webseite, welcher von `<body>`-*Tags* umschlossen wird.

### Zeichencodierung

Eine Codierung legt fest, wie genau der Inhalt einer Datei als Bitfolge gespeichert wird. Es gibt verschiedene Zeichencodierungen. Der Buchstabe `ü` wird in der ISO&nbsp;8859-1-Codierung in die Bitfolge `11111100` übersetzt, in der UTF-8-Codierung jedoch in die Bitfolge `11000011 10111100`. Decodiert man  diese Bitfolge jedoch mit ISO&nbsp;8859-1, so erhält man die beiden Zeichen `Ã¼`.

Deshalb ist es wichtig, in der HTML-Datei die verwendete Zeichencodierung festzuhalten. Dies geschieht mit `<meta charset="UTF-8">`. Dabei ist zu beachten, dass das `meta`-Element kein End-*Tag* besitzt.

### Titel der Webseite

Der Titel der Webseite wird mit `<title>`-*Tags* im Kopf der Webseite festgelegt. Der Titel wird vom Browser für verschiedene Zwecke verwendet:

- Darstellung in der Titelleiste des Browserfensters
- Bezeichnung des Tabs für die Webseite
- Name für Lesezeichen von des Webseite

Das `title`-Element muss sich innerhalb des `head`-Elements befinden. Jede Webseite muss zwingend ein `title`-Element haben.

``` html
<title>Titel der Webseite</title>
```

::: example
![Bildschirmfoto](images/sample-title.png)
:::

::: exercise Aufgabe 1 – Grundgerüst

Erstelle eine Webseite zu einem Thema, dass dich interessiert.

1. Öffne den Editor **Notepad++**.
2. Kopiere das Grundgerüst in den Editor.
3. Speichere die Datei als **index.html**.
4. Füge einen Titel, ein paar Überschriften und Text ein.
5. Öffne die Datei **index.html** mit einem Browser. Beobachte, was mit Zeilenumbrüchen im Browser geschieht.
6. Überprüfe die Datei mit dem [W3C Markup Validation Service](https://validator.w3.org/#validate_by_upload).

:::
