# Verknüpfungen
---

## Bilder

``` html
<img src="web/images/tim-berners-lee.jpg" alt="Bild von Tim Berners-Lee">
```

::: example
<img src="web/images/tim-berners-lee.jpg" alt="Bild von Tim Berners-Lee">
:::


Mit einem `img`-Element wird ein Bild eingefügt. `img` ist ein *Inline*-Element, ohne weitere Strukturierung werden somit Bilder direkt im Textfluss dargestellt.

Das `img`-Element hat keinen Inhalt und somit auch kein End-Tag.

Mit dem `src`-Attribut wird die URI der Bilddatei angegeben, mit dem `alt`-Attribut (engl. *alternative*) wird eine Beschreibung des Bildes angegeben. Die Angabe der Beschreibung mit `alt` ist nötig, sie wird angezeigt, wenn das Bild nicht verfügbar ist oder wenn es nicht angezeigt werden kann oder soll (z.B. in Spezialbrowsern für Menschen mit eingeschränkter Sehfähigkeit).

Mit dieser Methode werden Bilder als Inhalt in die Webseite eingefügt. Um Bilder als Dekoration (d.h. als Hintergrundbild) in eine Webseite einzubauen, sollte die CSS-Eigenschaft `background-image` verwendet werden.

## Links

Mit einem `a`-Element wird ein Hyperlink eingefügt. `a` ist ein *Inline*-Element.

Mit dem `href`-Attribut wird das Ziel des Links als URI angegeben. Der Inhalt des Elements wird als sichtbarer Teil des Links dargestellt. Als Inhalt kann beliebiger HTML-Code, beispielsweise auch ein Bild, dienen.

Um dem Browser mitzuteilen, dass ein Link in einem neuen Tab geöffnet werden soll, wird zusätzlich die Attributdefinition `target="_blank"` in das *Tag* gestellt.

``` html
Sie können das in den <a href="https://ict.mygymer.ch">ICT-Unterlagen</a> des
<a href="https://www.gymkirchenfeld.ch" target="_blank">Gymnasiums Kirchenfeld</a>
nachlesen.
```

::: example
Sie können das in den <a href="https://ict.mygymer.ch">ICT-Unterlagen</a> des
<a href="https://www.gymkirchenfeld.ch" target="\_blank">Gymnasiums Kirchenfeld</a> nachlesen.
:::

## Bilder mit Links

Um ein Bild mit einem Link zu versehen, muss das Bild als **Inhalt** des Links eingefügt werden.

``` html
<a href="https://www.gymkirchenfeld.ch">
  <img alt="Logo Gymnasium Kirchenfeld" src="web/images/logo-64x64.png">
</a>
```

::: example
<a href="https://www.gymkirchenfeld.ch">
  <img alt="Logo Gymnasium Kirchenfeld" src="web/images/logo-64x64.png">
</a>
:::

::: exercise Aufgabe 4 – Links und Bilder

Ergänze deine Webseite mit ein paar Bildern und Links.

Überprüfe die Datei mit dem [W3C Markup Validation Service](https://validator.w3.org/#validate_by_upload).
:::
