# :extra: 5 Webtechnologien
---

Nebst der E-Mail ist das **World Wide Web** die verbreitetste Anwendung des Internets. Als World Wide Web bezeichnet man die weltweit verknüpften Webseiten, welche man zu Gesicht bekommt, wenn man im «Internet surft». In diesem Lehrgang erfährst Du, wie eine Webseite technisch aufgebaut ist. Du lernst die grundlegenden Technologien kennen um den Aufbau einer Webseite zu verstehen und selbst eine Webseite erstellen zu können.

Eine Webseite ist aus einer Vielzahl von Dateien wie Bildern, Dokumenten oder Programmen aufgebaut. Das Grundgerüst einer Webseite ist eine HTML-Datei. Sie enthält den Bauplan der Webseite, der beschreibt, welche anderen Dateien zu der Webseite gehören und auf welche Weise sie zusammengesetzt werden sollen. Alle diese Dateien sind meist nicht auf dem eigenen Computer vorhanden, sondern werden bei Bedarf von einem Webserver angefordert.

Die drei wichtigsten Sprachen für die Gestaltung moderner Webseiten sind folgende:

::: cards 3
#### [:mdi-language-html5: HTML](?page=1-0-html)
Die **Hypertext Markup Language** ist eine **Auszeichnungssprache**, mit welcher der Inhalt und die Struktur einer Webseite definiert wird.

---
``` html
<html>
  <body>
    <h1>Hello</h1>
  </body>
</html>
```
***
#### [:mdi-language-css3: CSS](?page=2-0-css)
Cascading Stylesheets sind eine weit verbreitete Methode, das **Layout von Elementen** zu definieren. Mit CSS können Positionierung, Farbe, Hintergrundbilder, Abstände, Ausrichtung und vieles mehr festgelegt werden.

---
``` css
h1 {
  color: '#2345AB';
  margin-top: 1rem;
}
```
***
#### :mdi-language-javascript: JavaScript
JavaScript ist eine **Programmiersprache**, mit welcher interaktive Webseiten erstellt werden können.

---
``` javascript
function sayHello () {
  console.log('Hello');
};

window.onload(sayHello);
```
