# Textformatierung
---

## Horizontale Ausrichtung

Mit der CSS-Eigenschaft `text-align` wird die Textausrichtung beeinflusst.

| Wert      | Bedeutung                         |
| --------- | --------------------------------- |
| `left`    | linksbündig ausrichten (Standard) |
| `right`   | rechtsbündig ausrichten           |
| `center`  | zentrieren                        |
| `justify` | Blocksatz                         |

Das folgende Beispiel formatiert eine Webseite mit zentrierten Überschriften und Absätzen in Blocksatz:

``` css
h1 {
    text-align: center;
}
body {
    text-align: justify;
}
```

## Textdekoration

Mit der CSS-Eigenschaft `text-decoration` kann der Text über-, unter- oder durchgestrichen werden.

| Wert           | Bedeutung                   |
| -------------- | --------------------------- |
| `none`         | keine Dekoration (Standard) |
| `overline`     | überstrichener Text         |
| `line-through` | durchgestrichener Text      |
| `underline`    | unterstrichener Text        |

Unterstrichener Text sollte nur für Hyperlinks verwendet werden. Ansonsten kann dies Benutzer verwirren, da unterstrichene Textstellen üblicherweise als Hyperlinks interpretiert werden.

``` css
a {
    text-decoration: underline;
}
em {
    text-decoration: line-through;
}
```

## Schriftart

Die Schriftart wird mit der CSS-Eigenschaft `font-family` festgelegt. Da auf jedem Gerät unterschiedliche Schriftarten installiert sind, gibt es einen flexiblen Mechanismus, um eine passende Schrift auszuwählen.

Als Wert für `font-family` kann eine kommagetrennte Liste von Schriftarten angegeben werden. Der Browser verwendet die erste Schrift, welche auf dem System vorhanden ist. Dabei müssen Namen von Schriftarten mit Leerzeichen in doppelte Anführungszeichen gesetzt werden:

``` css
body {
    font-family: "Times New Roman", Roman, serif;
}
```

Ausserdem CSS sind vier allgemeine Namen für Schriftarten vorgegeben (siehe Tabelle). Jeder Browser muss diese Schriftarten auf eine verfügbare Systemschrift abbilden. Wenn also als letzte Schriftart in der Liste eine CSS-Standardschrift angegeben wird, ist sichergestellt, das der Browser immer eine geeignete Schriftart findet.

| Wert         | Bedeutung                                                     |
| ------------ | ------------------------------------------------------------- |
| `sans-serif` | Schrift ohne Serifen                                          |
| `serif`      | Schrift mit Serifen                                           |
| `fantasy`    | Schmuckschrift                                                |
| `monospace`  | Schrift mit gleichen Zeichenbreiten (Schreibmaschinenschrift) |

## Schriftgrösse

| Wert       | Bedeutung       |
| ---------- | --------------- |
| `xx-small` | sehr sehr klein |
| `x-small`  | sehr klein      |
| `small`    | klein           |
| `medium`   | mittel          |
| `large`    | gross           |
| `x-large`  | sehr gross      |
| `xx-large` | sehr sehr gross |

Für die Angabe der Schriftgrösse kann eine Grössenangabe wie `12pt` oder einer der Werte aus obenstehender Tabelle verwendet werden. Dabei ist `medium` die im Browser eingestellte, «normale» Schriftgrösse. Die anderen Werte ergeben relativ dazu eine entsprechend kleinere oder grössere Schrift.

``` css
h1 { font-size: 2cm; }
b { font-size: large; }
p { font-size: 12pt; }
```

## Fette und kursive Schrift

Um die Schrift fett darzustellen, wird die CSS-Eigenschaft `font-weight` auf den Wert `bold` gesetzt. Um die Schrift nicht fett darzustellen, wird der Wert `normal` gesetzt.

``` css
p { font-weight: bold; }
strong { font-weight: normal; }
```

Um die Schrift kursiv darzustellen, wird die CSS-Eigenschaft `font-style` auf den Wert `italic` gesetzt. Um die Schrift nicht fett darzustellen, wird der Wert `normal` gesetzt.

``` css
p { font-style: italic; }
em { font-style: normal; }
```

## Grössenangaben in CSS

Für Grössenangaben kennt CSS verschiedene Einheiten. Die wichtigsten sind in der nachfolgenden Tabelle aufgeführt. Für den Wert 0 sollte keine Einheit angegeben werden.

| Einheit | Bedeutung     | Grösse                                      |
| ------- | ------------- | ------------------------------------------- |
| `cm`    | Zentimeter    | 1 cm = 0.01 m                               |
| `mm`    | Millimeter    | 1 mm = 0.1 cm                               |
| `in`    | Zoll (_inch_) | 1 in = 2.54 cm                              |
| `pc`    | Pica          | 1 pc = 1/6 in                               |
| `pt`    | Punkt         | 1 pt = 1/12 pc                              |
| `%`     | Prozent       | relativ zum Elternelement                   |
| `px`    | Pixel         | abhängig von der Auflösung                  |
| `em`    | em            | relativ zur Schriftgrösse des Elements      |
| `rem`   | rem           | relativ zur Schriftgrösse des Root-Elements |


:::  exercise Aufgabe 8 – Textformatierung

Formatiere den Text deiner Webseite mit entsprechenden CSS-Regeln.

Überprüfe Deine Lösung mit dem W3C Markup Validation Service für [HTML](https://jigsaw.w3.org/css-validator/) und [CSS](https://jigsaw.w3.org/css-validator/)
:::
