# Textstruktur
---

## Absätze

Zur besseren Lesbarkeit wird ein Text bei der Gestaltung in **Absätze** unterteilt. In HTML wird ein Absatz als `p`-Element (engl. *paragraph*) dargestellt, also mit den *Tags* `<p>` und `</p>` umschlossen.

``` html
<p>
  Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod
  tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim
  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid
  ex ea commodi consequat.
</p>
<p>
  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse
  molestie consequat, vel illum dolore eu feugiat nulla facilisis at
  vero eros et accumsan et iusto odio dignissim qui blandit praesent
  luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
</p>
```

::: example
<p>
  Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.
</p>
<p>
  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
</p>
:::

## Überschriften

Die Elemente `h1` bis `h6` werden für die Bezeichnung von Überschriften von ganz gross (`h1`) bis ganz klein (`h6`) verwendet.

``` html
<h1>h1. Lorem Ipsum</h1>
<h2>h2. Lorem Ipsum</h2>
<h3>h3. Lorem Ipsum</h3>
<h4>h4. Lorem Ipsum</h4>
<h5>h5. Lorem Ipsum</h5>
<h6>h6. Lorem Ipsum</h6>
```

::: example
<h1>h1. Lorem Ipsum</h1>
<h2>h2. Lorem Ipsum</h2>
<h3>h3. Lorem Ipsum</h3>
<h4>h4. Lorem Ipsum</h4>
<h5>h5. Lorem Ipsum</h5>
<h6>h6. Lorem Ipsum</h6>
:::

## Zeilenumbruch

Ein Zeilenumbruch wird mit dem Element `<br>` (engl. *break*) markiert.

**Achtung:** Im Gegensatz zu den meisten anderen Elementen hat das `<br>`-Element keinen Inhalt und somit auch kein End-*Tag*.

``` html
Gymnasium Kirchenfeld<br>Kirchenfeldstrasse 25<br>3005 Bern
```

::: example
Gymnasium Kirchenfeld<br>Kirchenfeldstrasse 25<br>3005 Bern
:::

## Allgemeine Strukturierung

Für die allgemeine Strukturierung der Webseite wird häufig das `<div>`-Element (engl. *division*) verwendet. Es hat keine besondere Bedeutung, sondern dient dazu, dem Inhalt ein spezifisches Layout zuzuweisen.

::: exercise Aufgabe 2 – Textstruktur

Verwende `<h1>`, `<h2>`, `<p>` und `<br>`-Tags, um deine Webseite richtig zu strukturieren.

Überprüfe die Datei mit dem [W3C Markup Validation Service](https://validator.w3.org/#validate_by_upload).
:::
