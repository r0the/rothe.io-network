# Hintergrund
---

Wie schon erwähnt wird die Farbe des Hintergrunds mit der CSS-Eigenschaft `background-color` festgelegt.

Es ist auch möglich, ein Bild als Hintergrund zu verwenden. Dazu wird mit der Eigenschaft `background-image` die URL der Bilddatei angegeben. Die URL muss in der Form `url(...)` angegeben werden.  **Achtung:** Nach `url` muss direkt die Klammer folgen, es darf kein Leerzeichen vorkommen.

Mit der Eigenschaft `background-attachment` wird festgelegt, ob der Hintergrund mit der Webseite mitscrollen soll (Wert `scroll`) oder nicht (Wert `fixed`).

``` css
body {
    background-image: url(images/backgroundimage.png);
    background-attachment: scroll;
}
```

## Positionierung

Mit der CSS-Eigenschaft `background-position` kann das Hintergrundbild positioniert werden. Die folgende Grafik zeigt die möglichen Werte und deren Bedeutung:

![](images/background-position.svg)

Beispiel:

``` css
body {
  background-position: center;
}
```

## Kachelung

Mögliche Werte für die CSS-Eigenschaft `background-repeat` sind:

| Wert        | Bedeutung                           |
|:----------- |:----------------------------------- |
| `no-repeat` | keine Kachelung (Standard)          |
| `repeat`    | horizontale und vertikale Kachelung |
| `repeat-x`  | nur horizontale Kachelung           |
| `repeat-y`  | nur vertikale Kachelung             |

Mit einem relativ kleinen Kachelbild kann der ganze Hintergrund gefüllt werden. Ein gekacheltes Hintergrundbild passt sich automatisch der Grösse des Elements an. Die Kachelung wird mit der Eigenschaft `background-repeat` festgelegt. Dazu gibt es vier mögliche Werte (siehe Tabelle). `repeat-x` bzw. `repeat-y` kann mit einem sehr kleinen Bild eine durchgehende Verzierung erreicht werden, beispielsweise ein Farbverlauf.

``` css
body {
    background-image: url(red-gradient.png);
    background-repeat: repeat-y;
}
```

![Bilddatei mit Farbverlauf (red-gradient.png)](images/red-gradient.png)

![Resultierendes Hintergrundbild](images/background-tessellation-vertical.png)

::: exercise Aufgabe 9 – Hintergrund

Lege für Deine Webseite ein passendes Hintergrundbild fest.
:::
