# CSS-Syntax
---

``` css
Selektor {
    Eigenschaft: Wert;
}

Selektor {
    Eigenschaft: Wert;
    Eigenschaft: Wert;
}
```

Der **Selektor** gibt an, welche HTML-Elemente betroffen sind. Die **Eigenschaft** oder das **Attribut** gibt an, was festgelegt wird, z.B. die Schriftfarbe. Manche Eigenschaften sind nur für bestimmte HTML-Elemente definiert, Universalattribute gelten für alle Elemente. Der **Wert** ist eine für die entsprechende Eigenschaft erlaubte Angabe. Je nach Eigenschaft sind unterschiedliche Werte zugelassen, z.B. `red` für die Schriftfarbe.
