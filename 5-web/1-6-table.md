# Tabellen
---

Tabellen stehen in einem `<table>`-Element. Dort wird für jede Zeile ein `<tr>`-Element (engl. *table row*) eingefügt.

In jeder Zeile stehen wiederum Elemente, welche eine einzelne Zelle repräsentieren. Das ist entweder ein `<td>`-Element (engl. *table data*) oder ein `<th>`-Element (engl. *table heading*).

``` html
<table>
  <tr><th>Browser</th><th>Hersteller</th><th>Marktanteil</th></tr>
  <tr><td>Chrome</td><td>Google</td><td>68%</td></tr>
  <tr><td>Firefox</td><td>Mozilla Foundation</td><td>11%</td></tr>
  <tr><td>Internet Explorer</td><td>Microsoft</td><td>7%</td></tr>
  <tr><td>Safari</td><td>Apple</td><td>5%</td></tr>
  <tr><td>Edge</td><td>Microsoft</td><td>4%</td></tr>
</table>
```

::: example
<table>
  <tr><th>Browser</th><th>Hersteller</th><th>Marktanteil</th></tr>
  <tr><td>Chrome</td><td>Google</td><td>68%</td></tr>
  <tr><td>Firefox</td><td>Mozilla Foundation</td><td>11%</td></tr>
  <tr><td>Internet Explorer</td><td>Microsoft</td><td>7%</td></tr>
  <tr><td>Safari</td><td>Apple</td><td>5%</td></tr>
  <tr><td>Edge</td><td>Microsoft</td><td>4%</td></tr>
</table>
:::

::: exercise Aufgabe 6 – Tabelle
Füge eine Tabelle in deine Webseite ein.

Überprüfe die Datei mit dem [W3C Markup Validation Service](https://validator.w3.org/#validate_by_upload).
:::
