# CSS
---

## Inhalt und Layout

Mit HTML und CSS wird eine strikte Trennung von Inhalt und Layout umgesetzt. Anstatt dem Inhalt direkt eine Formatierung zuzuweisen, wird eine **Struktur** definiert. Den einzelnen Strukturelementen kann dann die Formatierung zugewiesen werden. So können sowohl Inhalt als auch Layout auf einfache Weise ausgetauscht werden.

Durch diese Trennung kann das gleiche Layout für viele unterschiedliche Inhalte (z.B. verschiedene einzelne Seiten einer umfangreichen Website) wiederverwendet werden. Andererseits kann der gleiche Inhalt einfach in verschiedenen Layouts dargestellt werden (z.B. als Webseite, als Druckvorlage, für mobile Geräte, usw.)

## Verknüpung mit HTML

Es gibt drei Möglichkeiten, CSS-Formatierungen mit HTML-Elementen zu verknüpfen:

- Verknüpfen der HTML-Datei mit einer (oder mehreren) CSS-Dateien.
- Definieren von CSS-Regeln in einem `style`-Element im Kopf (`head`) der HTML-Datei
- Definieren von CSS-Eigenschaften in einem `style`-Attribute in einem HTML-Element

::: warning
**CSS-Regeln sollten immer in einer separaten CSS-Datei definiert werden.**

Dies hat den Vorteil, dass HTML- und CSS-Code klar getrennt sind. Die gleiche CSS-Datei kann so für mehrere HTML- verwendet werden. Moderne Browser verlangen aus Sicherheitsgründen mittels *Content Security Policy* eine strikte Trennung von HTML und CSS.
:::

### Verknüpfung mit CSS-Dateien

Üblicherweise werden CSS-Regeln in einer separaten CSS-Datei definiert. Dies fördert einerseits eine saubere Trennung von Inhalt und Layout und ermöglicht andererseits das wiederverwenden der CSS-Regeln für mehrere Webseiten. Gerade bei umfangreichen Websites, welche aus vielen Einzelseiten zusammengesetzt sind, ist dies unabdingbar.

Eine CSS-Datei wird mit einem `link`-Element in Kopf der HTML-Datei eingebunden. Das Start-*Tag* muss die beiden Attribute `rel` und `href` enthalten. Der Wert von `rel` ist `stylesheet`, der Wert von `href` eine URI, welche auf die CSS-Datei verweist.

``` html
<!DOCTYPE html>
<html lang="de">
    <head>
        <link rel="stylesheet" href="mystyle.css">
        <title>CSS-Datei einbinden</title>
    </head>
    <body>
    </body>
</html>
```

### Definition im HTML-Kopf

Wenn CSS-Regeln nur für eine HTML-Datei gelten sollen, können sie auch direkt im Kopf der HTML-Datei innerhalb eines `style`-Elements definiert werden:

``` html
<!DOCTYPE html>
<html lang="de">
    <head>
        <style>
            body {
                background-color: rgb(100%, 90%, 100%);
            }
        </style>
        <title>CSS-Regeln im Kopf definieren</title>
    </head>
    <body>
    </body>
</html>
```
