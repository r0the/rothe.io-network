# Zeichenformatierung
---

| Start-*Tag* | Bedeutung             | Anwendung                                                                | übliche Darstellung  |
|:----------- |:--------------------- |:------------------------------------------------------------------------ |:-------------------- |
| `<em>`      | Betonung (*emphasis*) | spezielle Worte, z.B. Fachausdrücke, Eigennamen, anderssprachige Phrasen | kursiv               |
| `<strong>`  | starke Betonung       | bedeutungstragende Worte                                                 | fett                 |
| `<q>`       | Zitat (*quote*)       |                                                                          | in Anführungszeichen |

``` html
In seinem Buch <em>Weaving the Web</em> schreibt Tim
Berners-Lee über das <strong>World Wide Web</strong>: <q>The web is more a <em>social</em> creation than a
technical one.</q>
```

::: example
In seinem Buch <em>Weaving the Web</em> schreibt Tim
Berners-Lee über das <strong>World Wide Web</strong>: <q>The web is more a <em>social</em> creation than a
technical one.</q>
:::

::: exercise Aufgabe 3 – Zeichenformatierung

Ergänze deine Webseite mit ein paar Beispielen für Zeichenformatierung.

Überprüfe die Datei mit dem [W3C Markup Validation Service](https://validator.w3.org/#validate_by_upload).
:::
