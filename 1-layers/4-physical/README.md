# 1.4 Physikalische Schicht (Netzzugangsschicht)
---

Auf der phyikalischen Schicht geht es um das konkrete Transportmittel (Velokurier, Lastwagen, ... resp. WLAN, LAN, Mobilfunk, ...). Auch hier werden Adressen benötigt, es sind weltweit eindeutige Adressen, die fix an das Gerät geknüpft sind.

## Beispiel «Schule»

Im Beispiel «Schule» müssen die Säcke, die die vielen Pakete enthalten, nun endlich transportiert werden. Dies übernehmen Speditionsangestellte, die die Säcke mit Hilfe von Lastwagen an den nächsten Zielort (z.B. in ein Paketzentrum oder an die Lieferadresse) bringen. Die Spedition kümmert sich nur um den aktuellen Transportabschnitt.

![](./layer-1.svg)

## Internet

Die physikalische Schicht besteht aus der Hardware, also den Kabeln und Geräten (mit ihren physikalischen Adressen). Die physikalische Adresse wird **MAC**-Adresse genannt (siehe Begriff [MAC-Adressen](/?page=network/3-terms/2-mac/)).

Wieso braucht es zwei Adressen, logische Adressen (Vermittlungsschicht) **und** physikalische dieser Schicht? Innerhalb eines lokalen Netzwerks werden die physikalischen Adressen verwendet. Da es aber keine Möglichkeit gibt, alle physikalischen Adressen auf der Welt zu kennen **und** zu wissen, wie man diese Gerät erreichen kann, braucht es logische Adressen, die zu einem bestimmten Netzwerk gehören. Nur so ist es möglich, mit dem gleichen Gerät am Morgen zu Hause und später an der Schule online zu sein.

## :extra: Weitere Informationen

<VueVideo id="ZhEf7e4kopM"/>
