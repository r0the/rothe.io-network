# Meine Adressen
---

## Öffentlich sichtbare IP-Adresse ermitteln

Besuche die folgende Webseite, um die IP-Adresse zu ermitteln, unter welcher andere Computer im Internet dein Gerät «sehen»:

* [:link: whatismyip.org](https://www.whatsmyip.org/)

## :mdi-apple: Netzwerk-Adressen ermitteln auf iOS

1. Öffne die _Einstellungen_ und wähle __WLAN__.
2. Tippe beim aktuellen WLAN-Netzwerk auf das i-Symbol.
3. <del>Schaue unter _WLAN-Adresse_ nach, um deine **MAC-Adresse** zu ermitteln.</del> (Funktioniert erst ab iOS 14)
4. Schaue unter _IP-Adresse_ nach, um deine **IP-Adresse** zu ermitteln.

![](./network-config-ios.jpg)


## :mdi-android: Netzwerk/Adressen ermitteln auf Android

1. Öffne die _Einstellungen_ und scrolle ganz nach unten.
2. Wähle __Telefoninfo__.
3. Wähle __Status__.
4. Schaue unter _WLAN-MAC-Adresse_ nach, um deine **MAC-Adresse** zu ermitteln.
5. Schaue unter _IP-Adresse_ nach, um deine **IP-Adresse** zu ermitteln.

![](./network-config-android.jpg)
