# Routing[^1]
---

::: warning Vorgehen
Lies dich selbständig durch dieses Kapitel durch, dabei
- löst du die Aufgaben und notierst deine Ergebnisse,
- überlegst du dir vor jedem Kästchen «gewonnene Erkenntnisse» kurz, was eine gewonnene Erkenntnis sein könnte – und zwar bevor du das Kästchen aufklappst,
- liest du die «gewonnenen Erkenntnisse» durch und überlegst dir, was damit genau gemeint ist,
- notierst du allfällige Fragen oder eine Erkenntnis, die nicht aufgelistet wird, damit wir am Schluss darüber sprechen können.
:::

Wir befinden uns auf der Internetschicht: Hier werden IP-Pakete vom Start- zum Zielgerät transportiert. Bei ihrem Weg durch das Internet müssen die Pakete geleitet werden – man spricht von **Routing**.

## Bernmobil

Durch das Verkehrsnetz von Bernmmobil wird der öffentliche Verkehr in der Stadt Bern sichergestellt.

![Auszug Verkehrsnetz Bern-Mobil](./bernmobil.png)

::: exercise auf Papier
Du möchtest vom Gymnasium Kirchenfeld auf den Gurten reisen:

- Welche Möglichkeiten hast du?

Haltestellen, die du zu Fuss erreichen kannst, sind «Aegertenstrasse» und «Helvetiaplatz».
:::

::: exercise am Computer
Du möchtest vom Gymnasium Kirchenfeld auf den Gurten reisen:

- Welchen Weg schlägt dir [bernmobil.ch](https://bernmobil.ch) vor?
- Ändert sich der Vorschlag spät in der Nacht?
:::

::: details gewonnene Erkenntnisse

- Es gibt mehrere Wege, welche ans Ziel führen.
- Der optimale Weg kann sich ändern.
:::


### gefundene Analogien

- WLAN:  zu Fuss bis zur nächsten Haltestelle
- Sichtbare WLANs: Haltestellen, die zu Fuss erreichbar sind: Aegertenstrasse & Helvetiaplatz
- Rechnernetz (LAN/Internet): mit Bus, Tram oder Gurtenbahn
- Router: Umsteigen an gewissen Haltestellen

## Routing für Touristen

Datenpakete sind – anders als wir als Passagiere von *Bernmobil* – nicht intelligent. Sie wissen nicht, wie sie umsteigen müssen. Jemand muss sie die Datenpakete (um-)leiten.

Wir nehmen also an, wir seien Touristen und wollen vom Gymer auf den Gurten fahren. An den Haltestellen müssen wir also Informationen finden, ob und auf welchen Bus/welches Tram wir umsteigen müssen. Auch können wir uns keine künftigen Umsteige-Vorgänge merken, immer nur die aktuelle Haltestelle.

::: exercise Routing-Tabelle
Erstelle für das Ziel «Gurten» Umsteige-Anleitungen für die folgenden Haltestellen :

- Aegertenstrasse
- Helvetiaplatz
- Zytglogge
- Sulgenau
- Gurtenbahn

Beispiel-Anleitung

Hauptbahnhof: auf Linie 9 in Richtung Wabern umsteigen
:::

::: exercise andere Ziele
Die Anleitungen müssten natürlich auch Anweisungen haben für andere Ziele.

- Wie würde eine Umsteigungsweisung lauten für 6 obenstehenden Haltestellen für das Ziel «Zürich»?
- Wie für das Ziel «Genf»?
- Muss jede Haltestelle wirklich Anweisungen für jedes andere Ziel haben?
:::

::: details gewonnene Erkenntnisse
Es können Anweisungen «delegiert» werden: So muss nur der Hauptbahnhof wissen, in welchen Zug man einsteigen muss für Zürich oder Genf. Die bernmobil-Haltestellen leiten Reisende mit einem Zielort ausserhalb des bernmobil-Netzes an den Hauptbahnhof weiter.
:::

::: exercise Ausfall Schienennetz
Du möchtest vom Gymnasium Kirchenfeld auf den Gurten reisen:
Wegen einer grösseren Fehlfunktion stehen aber sämtliche Trams still. Busse verkehren weiterhin.

- Finde einen neuen Weg!
:::

::: details gewonnene Erkenntnisse
- Es gibt verschiedene Wege, auch normalerweise nicht optimale
- Dadurch existiert eine gewisse Ausfallsicherheit
:::


::: exercise Grosse Gruppe
4 Parallelklassen – also ca. 100 Personen inkl. LK – machen einen Ausflug zum Abendpicknick auf den Gurten. Wie kommen sie am schnellsten dahin, wenn wir damit rechnen, dass wegen Corona und dem Feierabendverkehr maximal 25 Personen pro Tram/Bus Platz finden?
:::

::: details gewonnene Erkenntnisse
- Bei Engpässen können verschiedene Wege gleichzeitig benutzt werden (dies nennt man *Load-Balancing*).
:::



## Routing für Datenpakete
Routing-Algorithmen sorgen dafür, dass Datenpakete ihren Weg durch das Internet finden. Dabei kann es sein, dass mehrere Pakete mit demselben Ziel unterschiedliche Routen nehmen.

![Bild: [Khanacademy](https://www.khanacademy.org/computing/computers-and-internet/xcae6f4a7ff015e7d:the-internet/xcae6f4a7ff015e7d:routing-with-redundancy/a/internet-routing) (CC-BY-NC-SA)](./routing.svg)

### Router im Schichtenmodell
Router stellen die Verbindung zwischen unterschiedlichen Netzwerken her. Sie müssen die IP-Pakete auspacken, damit diese gemäss der IP-Adresse weitergeleitet werden können. Dabei bedienen sich Router spezieller Tabellen, welche angeben, wohin ein Paket mit einer bestimmten Ziel-IP-Adresse hingeleitet werden soll.

![Bild: [Wikimedia Commons](https://de.wikipedia.org/wiki/Datei:IP-Paket_Routing_über_Netzwerke.svg)](./router.svg)

### Routen verfolgen
Mit dem Befehl `traceroute` (macOS) resp. `tracert` (Windows) kann diese Route nachverfolgt werden. Dabei werden die Zwischenstationen – also dort wo das Paket entpackt und gemäss Ziel-IP-Adresse weitergeleitet wird – angezeigt.

::: exercise traceroute
Öffne eine Eingabeaufforderung und gib die folgendene drei Befehl ein:

```
tracert admin.ad.kinet.ch
tracert www.google.ch
tracert www.gymkirchenfeld.ch
```

Beobachte den Output. Erkennst du Gemeinsamkeiten oder irgendeine speziellen Zwischenstation?
:::

## «Highspeed-Routen»

### Schweiz

Auf der untenstehenden Karte erkennt man die schnellsten Leitungen von Switch. Diese Organisation verbindet die Universitäten und Forschungsinstitute untereinander und mit dem Ausland.

![Bild: [Switch.ch](https://www.switch.ch/network/infrastructure/backbone/)](./switch.jpg)

Andere Provider besitzen ebenfalls schnelle Leitungen zwischen den Städten und ins Ausland. Z.B. wurden bestehende Gas-Leitungen mit schnellen Glasfaser-Kabeln versehen.

![Bild: [gas-com.ch](https://www.gas-com.ch/netzkarte/)](./gascom.png)

### Interkontinental

Zur Verbindung von Kontinenten hatte man schon vor dem Internet-Zeitalter Unterseekabel verlegt.

![Bild: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:1901_Eastern_Telegraph_cables.png)](./eastern-telegraph-cables.png)



Heute laufen zahlreiche «Highspeed-Routen» über den Grund der Meere.


<VueVideo id="0TZwiUwZwIE"/>

::: exercise Unterseekabel
Wie kommt ein Datenpaket von Bern

- in die USA?
- nach Madagaskar?

* [:link: https://www.submarinecablemap.com/](https://www.submarinecablemap.com/)
:::

[^1]: Quelle: [Sebastian Forster](https://informatik.mygymer.ch/g23c/008.rechnernetze-kommunikation/05.routing.html)
