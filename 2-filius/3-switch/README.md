# 2.3 Switch[^1]
---

<VueVideo id="_LSYWZRcfM4"/>

::: info
#### :mdi-lightbulb-on-outline: Switch
Ein Switch ist ein Gerät mit vielen Netzwerkanschlüssen, um Rechner untereinander zu verbinden. Da jeder Rechner in der Regel nur über einen solchen Anschluss verfügt, ist der Einsatz von Switches notwendig, um mehr als zwei Rechner miteinander zu verbinden. In grösseren Netzen sind mehrere Switches sinnvoll. In der Regel werden diese in Form eines Baums angeordnet.
:::

::: exercise Aufgabe 3
Erweitern wir nun unser Netzwerk.
1. Füge einen dritten Computer – einen Server – mit dem Namen **Server 0.12** und der IP-Adresse `192.168.0.12` hinzu.
2. Verbinde alle Computer mit einem Switch wie abgebildet.

   ![](./filius-switch-1.png)

3. Versuche, von jedem Rechner aus die anderen beiden zu pingen.
4. Wie würde man einen Switch nennen, wenn wir von einem Stromnetz sprechen würden, anstatt von einem Computernetzwerk? Halte die Antwort im Dokumentationsmodus in Filius **schriftlich** fest.
5. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-03.fls_ ab.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
