# 2.4 Client-Server-Prinzip[^1]
---

<VueVideo id="8THLsQDjHxE"/>

::: exercise Aufgabe 4
Hallo! Echo!
1. Installiere auf dem Server 0.12 einen _Echo-Server_ und starte diesen auf dem voreingestellten Port 55555.
2. Installiere auf dem Client Rechner 0.10 einen _Einfachen Client_ und verbinde diesen mit dem Echo- Server. (Achtung: richtige IP-Adresse eingeben!)
3. Sende vom Client (Rechner 0.10) einige Textnachrichten und beobachte den Effekt. Schaue dir auch die Netzwerkaktivität im Datenaustausch-Fenster des Clients an.
4. Was ist die Aufgabe eines Echo-Servers?
5. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-04.fls_ ab.
:::

::: info
### :mdi-lightbulb-on: Client - Server Prinzip
Du hast sicherlich herausgefunden, dass die Aufgabe des Echo Servers ist, dem «einfachen Client» zu antworten.

Das ist auch die grundlegende Idee des **Client-Server-Prinzips: Der *Client* (dt: Kunde) fordert beim *Server* (dt. Bediener, Anbieter) einen Dienst an. Dieser Dienst wird dann vom Serverprogramm erledigt.** So können von einem Client auch mehrere Dienste bei verschiedenen Serverprogrammen auf verschiedenen Rechnern angefordert werden. Der Vorteil ist dabei, dass die Leistung des Clients dadurch nicht so stark beeinträchtigt wird.

Server-Beispiele sind: Datei-Server, Druck-Server, Web-Server, Mail-Server, User-Server, Config-Server usw.

Unser Beispiel: Der Client fordert das exakte Zurücksenden der Nachricht an, der Echo-Server erfüllt diesen Auftrag.

Damit die Kommunikation funktioniert, muss für jeden Dienst ein bestimmtes **Regelwerk** vereinbart werden. Dieses nennt man *Protokoll*.

Es ist wichtig zu beachten, dass mit Server theoretisch nur die Server-Programme bezeichnet werden. Häufig nennt man aber auch spezielle Computer Server. Das tut man aber nur, da diese Rechner nur dazu benutzt werden, um Server-**Programme** auszuführen und sonst nichts anderes.
Du wirst im Verlauf dieses Kurses noch weitere Server kennen lernen.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
