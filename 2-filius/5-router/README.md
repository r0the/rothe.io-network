# 2.5 Router[^1]
---

<VueVideo id="pWO_fSIt6Ag"/>

![](./filius-router-1.png)


::: info
#### :mdi-lightbulb-on: Router = Vermittlungsrechner

Wir kommen nun in die Situation, dass wir zwei Netzwerke miteinander verbinden wollen. Zum Beispiel könnten wir das Netzwerk bei uns zu Hause mit dem Netzwerk der Schule verbinden wollen, um dort auf die Dateien zuzugreifen.

Wenn man Signale aus einem Netzwerk «0» in ein anderes Netzwerk «1» versenden möchte, dann benötigt man einen Router/Vermittlungsrechner. Ein Router verbindet mehrere Netzwerke. Ein Router befindet sich häufig an den Aussengrenzen eines Netzwerks, um es mit dem Internet oder einem anderen Netzwerk zu verbinden. Über die Routing-Tabelle entscheidet ein Router, welchen Weg ein Datenpaket nimmt.

Zu Anfang fragt Filius, wie viele Schnittstellen der Vermittlungsrechner bereitstellen soll. In unserem Fall reichen erstmal 2.

Die eingestellte Anzahl kann später in den Einstellungen des Vermittlungsrechners (Doppelklick auf den Vermittlungsrechner) unter der Registerkarte _Allgemein_ verändert werden.
:::

::: exercise Aufgabe 5
Netzwerke verbinden mit einem Router
1. Erstelle ein weiteres Netzwerk mit drei Rechnern und einem Switch. Die neuen Rechner sollen sich in einem logisch (=IP Bereich) anderen Netzwerk befinden. Wähle dafür die IP-Adressen `192.168.1.10` bis `192.168.1.12` und die entsprechenden Namen.
2. Füge nun einen Router dazwischen ein. Wähle 2 Schnittstellen aus. ⇒ Siehe Bild
3. Der Router braucht nun auch 2 IP-Adressen:
   - `192.168.0.1` auf der Seite des Netzwerks 0.
   - `192.168.1.1` auf der Seite des Netzwerks 1.
4. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-05.fls_ ab.
5. Wechsle in den Aktionsmodus:
   - Öffne auf Rechner 0.10 die Befehlszeile
   - Teste die Verbindung indem du einen Ping sendest an IP-Adresse `192.168.1.10`

    Ist der Ping nicht erfolgreich? Dann stimmt alles! Abwarten...

6. Benutzt ihr zu Hause einen Router? Wofür wird er verwendet?

***

[Musterlösung](./aufgabe-05.fls)

:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
