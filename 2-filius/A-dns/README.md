# 2.10 DNS-Server[^1]
---

<VueVideo id="-pqEJJbJcBA"/>

::: info
#### :mdi-lightbulb-on: Domain Name System (DNS-Server)
Kennst du die Handynummern all deiner Freunde auswendig? Wahrscheinlich nicht. Du hast sie einfach in deinem Handy abgespeichert und wenn du sie anrufen willst, musst du nur ihren Namen suchen. Denn Namen kann man sich viel leichter merken als lange Zahlenkombinationen.

Das gleiche Problem haben wir im Internet. Jeder Webserver hat eine IP, unter der man ihn erreicht. So ist das auch in unserem kleinen, nachgebauten Internet. Diese langen Zahlenkolonnen kann man sich aber schlecht merken – also nutzt man Namen. Diese Namen nennt man Webadresse oder auch «URL» (Uniform Resource Locator). Sie lauten zum Beispiel **www.gymkirchenfeld.ch** oder **www.wikipedia.ch**.

Anstatt eines Telefonbuches benutzt man das **Domain Name System** (DNS). Diese Zuordnung (also dass jemand, der `www.gymkirchenfeld.ch` eingibt, dann auf der richtigen IP landet) erledigt ein DNS-Server. Er kennt die IP der Webseite. Wenn jemand in seinem Browser die URL eingibt, wird beim DNS-Server nachgefragt: Welche IP-Adresse soll ich aufrufen? Der DNS Server antwortet und der Client kann die gewünschte IP-Adresse kontaktieren.
:::

::: exercise Aufgabe 10
DNS-Server einrichten
1. Im Bearbeitungsmodus:
   - Doppelklick auf den Router/Vermittlungsrechner.
   - Klicke auf _Verbindungen verwalten_
   - Klicke rechts auf das _+_ unten, um einen weiteren Anschluss hinzuzufügen.
   - Ändere die IP-Adresse des neuen Anschlusses auf `192.168.2.1`

   ![](./filius-dns-2.jpg)

2. Erstelle einen neuen Server
   - Ändere den Namen auf _DNS-Server 2.10_
   - Ändere die IP auf `192.168.2.10`
   - Stelle das Gateway auf die IP `192.168.2.1`
3. Verbinde den DNS-Server mit dem Vermittlungsrechner/Router mit einem Kabel.
4. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-10.fls_ ab.
5. Mit welchem Alltagsgegenstand lässt sich ein DNS vergleichen?

![](./filius-dns-1.jpg)

:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
