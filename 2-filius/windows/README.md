# :mdi-microsoft-windows: Installation auf Windows
---

Lade das Installationsprogramm herunter und führe es aus:

* [:mdi-microsoft-windows: Filus 1.12.4 für Windows][1]

[1]: https://www.lernsoftware-filius.de/downloads/Setup/Filius-Setup_with-JRE-1.12.4.exe
