# 2.9 Webserver testen[^1]
---

<VueVideo id="XS0TcLHu1K4"/>

::: exercise Aufgabe 9
Im privaten Web surfen.
1. Öffne Rechner 1.10 und installiere einen Webbrowser.
2. Starte den Browser und gib in die Adresszeile die URL _http://192.168.0.12_ ein. Jetzt solltest du die Webseite sehen, die wir erstellt haben. Klicke auf den Link, um sicherzustellen, dass auch die Seite mit den Kontaktangaben funktioniert.
3. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-09.fls_ ab.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
