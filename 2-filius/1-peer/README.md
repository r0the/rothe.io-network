# 2.1 Peer-to-Peer-Netzwerk[^1]
---

<VueVideo id="nlFXxrVOMGQ"/>

![](./filius-peer-1.png)

::: info
#### :mdi-lightbulb-on-outline: Direkte Vernetzung
Die einfachste Art, zwei Rechner miteinander zu vernetzen, besteht darin, Rechner direkt mit einem Netzwerkkabel zu verbinden. Man spricht hier von einer Peer-to-Peer-Vernetzung. In der Praxis können so zwei Rechner direkt mit einem Netzwerkkabel verbunden werden, so dass ein Datenaustausch der beiden Rechner möglich ist.
:::

::: exercise Aufgabe 1
Erstelle ein Netzwerk mit zwei vernetzten Computern, welche beide eine Client-Funktion haben.

Die Computer sollen passend zu den IP-Adressen die Namen **Rechner 0.10** und **Rechner 0.11** haben sowie die IP-Adressen `192.168.0.10` und `192.168.0.11` besitzen.

**Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-01.fls_ ab.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
