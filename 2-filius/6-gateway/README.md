# 2.6 Gateway[^1]
---

<VueVideo id="iI-u1ovOUQo"/>

> Stell dir vor, du möchtest deine Freundin Franziska besuchen.
>
> Wenn du sie sehen willst, musst du zu ihr hinlaufen. Du fängst also an und schaust bei dir zu Hause und öffnest die Tür zum Wohnzimmer: nichts. Hinter der Tür der Küche: Keine Franziska. Und im Bad: Ebenfalls niemand. Hinter all diesen Türen kannst du sie nicht finden. Das ergibt auch Sinn, denn Franziska wohnt nicht bei dir zu Hause.
>
> Also musst du eine Tür finden, durch die du gehen kannst, um sie zu finden. Na klar: Die Haustür.
>
> Du gehst also durch die Haustür und dann zu Franziskas Haus.
>
> Dort angekommen gehst du durch ihre Haustür und durch die Tür in ihr Zimmer. Und siehe da: Dort ist Franziska!

::: columns 2
![](./filius-gateway-1.png)
***
![](./filius-router-1.png)
:::


::: info
#### :mdi-lightbulb-on: Gateway – die Haustür
Verbindet man mehrere Netzwerke (z.B. Netzwerk 0 und Netzwerk 1) miteinander, z.B. mit dem Internet, so muss ein Router/Vermittlungsrechner genutzt werden. Das weisst du schon von gerade eben.

Nun müssen die Rechner in einem LAN (Local Area Network) aber wissen, wie sie aus diesem LAN «herauskommen» und auf das WAN (Wide Area Network) zugreifen können. Dazu müssen sie eine «Durchgangs-IP» oder *Gateway-IP* kennen, quasi die Haustür. An diese «Durchgangs-IP» müssen sich die Rechner wenden, wenn die angefragte IP im LAN nicht vorhanden ist. Diese «Durchgangs-IP» nennt sich *Gateway* oder *Gateway-IP*.

Die Gateway-IP ist normalerweise die IP des Routers/Vermittlungsrechners, da dieser ja eben mehrere Netzwerke miteinander verbindet.

Machen wir es an unserem Beispiel konkret:

Ich will von meinem Rechner 0.10 im Netzwerk «0» den Rechner 1.10 im Netzwerk «1» anpingen. Das klappt nicht, da es im Netzwerk 0 keinen Rechner 1.10 gibt.

Also sage ich dem Rechner 0.10 «Wenn du eine IP anpingen willst, die es in unserem Netzwerk nicht gibt, dann gehe über die Durchgangs-IP/Gateway-IP in die verbundenen Netzwerke und frage da nach!»
:::

Lege zunächst im Vermittlungsrechner/Router die beiden IP Adressen fest. Dazu solltest du auf die grün leuchtenden Verbindungskabel achten!

1. Doppelklick auf den Vermittlungsrechner.
2. Die IPs der beiden Anschlüsse setzen auf: `192.168.0.1` und `192.168.1.1`

![](./filius-gateway-3.jpg)

::: exercise Aufgabe 6
Der Ping in der vorherigen Aufgabe war nicht erfolgreich. Das Problem liegt darin, dass der Ping in ein anderes Netzwerk ging.

Allerdings ist bei den einzelnen Rechnern noch kein Gateway eingestellt, welches bestimmt, wohin die Nachrichten geschickt werden sollen, die nicht im Netzwerk verbleiben sollen.

1. Eine IP-Adresse des Routers lautet `192.168.0.1`. Diese Adresse fügst du als Gateway bei den drei Computern im Netzwerk 0 hinzu.
2. Die IP-Adresse im anderen Netzwerk lautet `192.168.1.1`, diese wird als Gateway für die drei Computer im Netzwerk 1 verwendet.
3. Prüfe anschliessend im Aktionsmodus die Verbindung mit einem Ping Befehl von Rechner 0.10 zum Rechner 1.10.
4. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-06.fls_ ab.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
