# 2.8 Webserver einrichten[^1]
---

<VueVideo id="rmGOfuXW_nI"/>

::: exercise Aufgabe 8

Mein eigenes «Web».
1. Installiere auf dem Server 0.12 einen Webserver und einen Texteditor.
2. Öffne mit dem Texteditor die Datei __index.html__. Du findest sie im Verzeichnis
__root/webserver__.
3. Gib den gesamten Code aus dem Feld __index.html__ unten ein. Achtung ⇒ jedes
Zeichen ist wichtig!
4. Speichere das Dokument ab und schliesse es.
5. Erstelle ein weiteres Dokument, das du als __kontakt.html__ speicherst.
6. Gib den Text unten als Inhalt für __kontakt.html__ ein, speichere ab und schliesse den Editor.
7. Öffne die Software Webserver mit einem Doppelklick und «Starte» den Webserver.
8. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-08.fls_ ab.
:::

#### Datei index.html

```html
<html>
  <head>
    <title>Standardseite</title>
  </head>
  <body bgcolor="#ccddff">
    <h2> Meine eigene Seite! </h2>
    <p>Herzlich Willkommen auf meiner Webseite</p>
    <p> Ich freue mich, dass Sie diese Seite besuchen. </p>
    <p><a href=kontakt.html>Kontaktseite</a></p>
  </body>
</html>
```

#### Datei kontakt.html

```html
<html>
  <head>
    <title>Kontakt</title>
  </head>
  <body bgcolor="red">
    <h1> Meine Kontaktseite </h1> <p>Name: Superhacker</p>
    <p> Telefon: 0196336995</p>
    <p> Email: superhacker@web.de</p>
  </body>
</html>
```

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
