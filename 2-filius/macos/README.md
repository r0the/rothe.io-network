# :mdi-apple: Installation auf macOS
---

## Java installieren

Damit du Filius auf macOS starten kannst, muss du erst Java installieren:

* [:download: Java JRE 14.0.2 für macOS herunterladen][1]

::: details Java-Installation Schritt für Schritt

1. Klicke hier auf __Fortfahren__:

    ![](./macos-java-install-1.png)

2. Klicke auf __Fortfahren__ und anschliessend auf __Akzeptieren__:

    ![](./macos-java-install-2.png)

3. Klicke auf __Installieren__:

    ![](./macos-java-install-3.png)

4. Bestätige die Installation mit deinem Fingerabdruck oder klicke auf __Passwort&nbsp;verwenden&nbsp;…__:

    ![](./macos-confirm-installation.png)

5. Klicke auf __Schliessen__, anschliessend auf __In&nbsp;den&nbsp;Papierkorb&nbsp;legen__:

    ![](./macos-java-install-4.png)

:::

## Filius installieren und ertmals starten

1. Lade die Filius ZIP-Datei herunter:

* [:download: Filus 1.12.4 für macOS][2]

2. Öffne den neu erstellten Ordner __filius-1.12.4__ und starte das Programm __filius.jar__.

    ![](./macos-filius-install-1.png)

4. Da Filius keine *signierte* Anwendung ist, ist der erste Start ein wenig umständlich. Beim ersten Start erscheint folgende Meldung:

    ![](./macos-filius-install-2.png)

5. Öffne die _Systemeinstellungen_, wähle _Sicherheit_ und dort das Tab _Allgemein_. Überprüfe, ob in der unteren Hälfte wirklich «filius.jar» steht und klicke dann auf _Dennoch öffnen_:

    ![](./macos-filius-install-3.svg)

6. Klicke auf _Öffnen_:

    ![](./macos-filius-install-4.png)

7. Klicke auf _OK_:

    ![](./macos-filius-install-5.png)

[1]: https://github.com/AdoptOpenJDK/openjdk14-binaries/releases/download/jdk-14.0.2%2B12_openj9-0.21.0/OpenJDK14U-jre_x64_mac_openj9_14.0.2_12_openj9-0.21.0.pkg
[2]: https://www.lernsoftware-filius.de/downloads/Setup/filius-1.12.4.zip
