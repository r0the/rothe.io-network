# 2.11 DNS auf Clients[^1]
---

<VueVideo id="zoXKPYcF2Dk"/>

::: exercise Aufgabe 11
DNS Server auf den Clients einrichten
1. Im Bearbeitungsmodus:
   - Trage bei jedem Rechner und beim Server 0.12 die IP Adresse des DNS-Servers in den Einstellungen ein: `192.168.2.10`

   ![](./filius-dns-3.jpg)

2. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-11.fls_ ab.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
