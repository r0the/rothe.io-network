# 2.7 Das Web[^1]
---

<VueVideo id="_e90JGg-BTg"/>

::: info
#### :mdi-lightbulb-on: Das «Internet» Nur mit einem Webserver.
Jetzt wollen wir unser eigenes kleines Internet erstellen.

Wir kennen das Internet aus dem Webbrowser. Dort geben wir Adressen (wie www.web.de) ein und sehen «bunte Seiten».

Das wollen wir so «nachbauen». Dafür benötigen wir eine Software, die sich «Webserver» nennt.

**Kurzinfo zu Webservern:**
Ein Webserver ist eine Software. Ihre Aufgabe ist es, Dokumente und Dateien an einen Client zu übertragen. Der Standardclient für einen Webserver ist ein Webbrowser. Die kennst du sicher: Firefox, Chrome usw.

Webserver werden lokal, in Firmennetzwerken und überwiegend als WWW-Dienst im Internet eingesetzt.
Die Hauptaufgabe von Webservern ist es, Dateien bereitzuhalten, die Webseiten beinhalten. Diese nennt man i.d.R. «HTML-Dateien». HTML steht für *Hypertext Markup Language*, also einer Auszeichnungssprache für Texte, die mit Links verknüpft werden können.

In HTML-Dateien wiederum können Bilddateien, Videodateien uvm. verlinkt und angezeigt werden.

Für eine komplette Webseite werden in der Regel die HTML-Seite inklusive verknüpfter
1. Designbeschreibungen (CSS) und
2. Bilddateien (JPG, PNG, GIF, SVG)
jeweils als einzelne Dateien übertragen.

Für jede benötigte Datei muss der Webbrowser eine eigene Anfrage an den Webserver senden, d. h. zur Darstellung einer komplexen Webseite sind manchmal hunderte Anfragen und Serverantworten nötig.

Ein Webserver ist in der Lage, die Inhalte einer Webseite gleichzeitig auf viele verschiedene Rechner auszuliefern.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
