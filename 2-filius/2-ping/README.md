# 2.2 Ping[^1]
---

<VueVideo id="iZ8l-DkckZE"/>

::: exercise Aufgabe 2

Jetzt wollen wir das Mini-Netzwerk testen:
1. Installiere auf dem **Rechner 0.10** eine _Befehlszeile_.
2. Starte die Befehlszeile und teste die Verbindung zum **Rechner 0.11** mit dem Befehl `ping 192.168.0.11`.
3. Beobachte die Netzwerkaktivität, indem Du dir den Datenaustausch von **Rechner 0.10** anzeigen lässt (siehe Infobox unten).
4. Teste auch andere Befehle auf der Befehlszeile, wie z. B. die Befehle `ipconfig`, oder `dir`.
5. Was passiert, wenn du den Befehl `ping 192.168.0.12` ausführst? Überlege, bevor du es ausprobierst und prüfe anschliessend im Datenaustausch-Fenster, ob du richtig liegst.
6. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-02.fls_ ab.
:::

::: info
#### :mdi-lightbulb-on-outline: Datenaustausch-Fenster
Zum Öffnen: Im Aktionsmodus einen Rechtsklick auf den Rechner/Laptop machen.

Bei zukünftigen Aufgaben im Aktionsmodus solltest du immer wieder den Datenaustausch im Datenaustausch-Fenster anschauen, um zu verstehen, welche Informationen die jeweilige Anwendung tatsächlich über das Netz schickt. Du wirst z. B. bei der Versendung von E-Mails erkennen, welche Datenflut über das Netzwerk verschickt werden muss.

![](./filius-log.png)
:::

### Der PING Befehl

::: info
#### :mdi-lightbulb-on-outline: Ping / Pong
Um zu überprüfen, ob ein anderer Rechner im Netzwerk «angerufen» bzw. erreicht werden kann, nutzt man den Pingbefehl und die IP-Adresse des Rechners:

```
ping 192.168.0.11
```
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
