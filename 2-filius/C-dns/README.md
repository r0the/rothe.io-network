# 2.12 DNS testen[^1]
---

<VueVideo id="FtsSm5zSL-c"/>

::: exercise Aufgabe 12
1. Im Aktionsmodus: Auf dem DNS-Server 2.10
   - Installiere die Software „DNS-Server“
2. Öffne die Software DNS-Server und gib folgendes ein:
   - Domainname: `www.willkommen.ch` (Achtung, hier wird automatisch ein Punkt dahinter gesetzt, das ist ok so.)
   - IP-Adresse: `192.168.0.12`
   - Klicke auf _Hinzufügen_.
   - Klicke auf _Starten_, um den DNS-Server zu starten.

   ![](./filius-dns-4.jpg)

3. Öffne nun auf dem Rechner 1.10 den Webbrowser. Öffne dort die Seite __http://www.willkommen.ch__. Du solltest nun auf die programmierte Webseite weitergeleitet werden.
4. Jetzt wollen wir noch einen Befehl test:
   - Öffne die Befehlszeile.
   - Gib den Befehl __host www.willkommen.ch__ ein. Du solltest nun die richtige IP Adresse angezeigt bekommen.

   ![](./filius-dns-5.jpg)

5. **Abschluss:** Bitte speichere die fertige Aufgabe unter dem Namen _Aufgabe-12.fls_ ab.
6. Erkläre die Funktionsweise eines DNS-Servers.
:::

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
