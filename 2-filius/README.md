# 2 Filius-Workshop[^1]
---

## Einleitung

Dieser Workshop ist so aufgebaut, dass zu einem Thema jeweils ein Video vorhanden ist. Das Video leitet dich durch die Schritte, die du vornehmen sollst und erklärt dazu einige Inhalte.

Das heisst, alle Schritte, die im Video gemacht werden, sollst du in deiner eigenen Simulation ebenfalls durchführen und ausprobieren. Dabei kannst du am besten nachvollziehen und beobachten, was passiert.

Deine eigene Netzwerkumgebung solltest du nach jedem Kapitel **speichern**.

Im Anschluss werden dir weitere Aufgaben oder Fragen gestellt, die du beantworten sollst, bevor du mit dem nächsten Schritt weiter machst.

## Installation

Folge der entsprechenden Anleitung, um Filius auf deinem Laptop zu installieren:

* [:mdi-microsoft-windows: Filus auf Windows installieren](?page=windows/)
* [:mdi-apple: Filus auf macOS installieren](?page=macos/)
* [:link: Filius-Webseite][1]

## Der Start(bildschirm)

<VueVideo id="UJHENkauCDc"/>

[1]: https://lernsoftware-filius.de/

[^1]: Quelle: Adrian Sauer (2020), [Interaktiver Kurs zu Rechnernetzen](https://www.tutory.de/w/c4ae6cde), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
