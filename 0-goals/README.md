# Lernziele
---

::: goal Lernziele Schichtenmodell
- Du kannst den typischen physikalischen Weg einer WhatsApp-Nachricht skizzieren.
- Du kannst die Netzwerkkommunikation bei einem Aufruf einer Webseite in einem Browser skizzieren.
- Du kennst die vier Schichten des vereinfachten Schichtenmodells.
- Du kannst anhand einer Analogie die vier Schichten des Schichtenmodells erklären und auf Computernetzwerke übertragen.
- Du weisst, wozu Protokolle verwendet werden und kannst die Protokolle DNS, HTTP, TCP, IP der richtigen Schicht zuordnen.
- Du erkennst Probleme in einem Netzwerk und kannst sie der entsprechenden Schicht zuordnen.
- Du kannst erklären, wieso in der Informatik Schichtenmodelle verwendet werden.
:::

::: goal Lernziele Filius
Du wendest die Kenntnisse aus der Theorie in der Software Filius an. Insbesondere kannst Du…
- ein einfaches Netzwerk mit mehreren Subnetzen entwerfen,
- DNS- und Webserver starten und konfigurieren,
- ein Netzwerk mit den vorhandenen Tools (Eingabeaufforderung, Webbrowser) testen,
- Fehler in einem Netzwerk finden und beheben.
:::

::: goal Lernziele Begriffe
Du kennst die folgenden Bezeichnungen, kannst sie erklären und korrekt verwenden:
- Domain Name, Host, Hostname
- IP-Adresse, MAC-Adresse
- Server, Client
- Switch, Access Point, Router
- Netzwerk, Internet, LAN, WLAN
- Protokoll, HTTP, DNS, TCP, IP
:::

## Grundlagen

Diese Unterrichtseinheit deckt die folgenden Grobziele und Inhalte aus dem kantonalen Lehrplan[^1] ab:

> ### Systeme und Sicherheit
>
> #### Grobziele
> Die Schülerinnen und Schüler
> - kennen den Aufbau und die grundlegende Funktionsweise von […] Netzwerken
> - verstehen wichtige für die Informatik relevante Messgrössen und Leistungsmerkmale
> - können erklären, wie Kommunikation zwischen Computern anhand von Protokollen funktioniert
>
> #### Inhalte
> - Wichtige Kenngrössen (z.B. Bit, Byte […], Gbps)
> - Beispiel eines Protokolls (z.B. HTTP/HTTPS, DNS)
> - Netzwerkdienste

[^1]: Quelle: *[Lehrplan 17 für den gymnasialen Bildungsgang - Informatik][1], Erziehungsdirektion des Kantons Bern, S. 145 - 146*

[1]: https://www.erz.be.ch/erz/de/index/mittelschule/mittelschule/gymnasium/lehrplan_maturitaetsausbildung.html
