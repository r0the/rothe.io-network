# MAC-Adresse
---

## MAC-Adresse

Die physikalische Adresse, auch Hardware-Adresse eines Computers – genauer gesagt: seiner Netzwerkschnittstelle – nennt man MAC-Adresse (das hat nichts mit Apple zu tun, sondern steht für *media access control*). Sie besteht aus 48 Bit, die man üblicherweise als Hexadezimalzahl mit 6×2 Stellen angibt, beispielsweise: `48:2C:6A:1E:59:3D`. Jede MAC-Adresse muss weltweit eindeutig sein, sie wird schon bei der Herstellung der Netzwerkkarte fix eingebaut.

MAC-Adressen sind essentiell für die digitale Kommunikation, weil Datenpakete ausschliesslich von einer Netzwerkkarte an eine andere weitergegeben werden können, also von einer MAC-Adresse an eine andere.
