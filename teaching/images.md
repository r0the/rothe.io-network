# Bilder
---

::: columns 4
![Postsack](images/bag.svg)

***
![Brief](images/letter.svg)

***
![Hauswart](images/mechanic.svg)

***
![Paket](images/packet.svg)

***
![Schüler](images/school-kids-1.svg)

***
![Schüler](images/school-kids-2.svg)

***
![Pult](images/student-desk.svg)

***
![Lastwagen](images/truck.svg)
:::
