# Filius Advanced
---

::: warning Achtung
Diese Aufgaben sind sehr komplex!
:::


Falls ihr euch noch intensiver mit Netzwerken beschäftigen möchtet, gibt es hier zusätzliches Material:

## Netzwerkmasken verstehen

* [:download: Filius-Datei: Netzwerkmasken verstehen](1-netzwerkmasken.fls)

Die Filius-Datei erklärt, wie Netzwerkmasken funktionieren und bietet einige Aufgabe dazu.

## Funktionsweise des Internets

* [:download: Filius-Datei: Funktionsweise des Internets](2-internet-dns-mail.fls)
* [:pdf: Anleitung und Aufgaben zu Funktionsweise des Internets](./2-internet-dns-mail.pdf)
* [:docx: Anleitung und Aufgaben zu Funktionsweise des Internets](./2-internet-dns-mail.docx)

Die Filius-Datei enthält mehrere Netzwerke mit diversen DNS-Servern, Root-DNS-Servern sowie Mailservern aus verschiedenen Domains. Das beiliegende Dokument enthält Erklärungen und Aufgaben dazu. Die Aufgaben helfen, die Funktionsweise des Internets zu verstehen.
