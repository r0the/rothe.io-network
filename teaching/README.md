# Material für Lehrer*innen
---

## 1. Doppellektion – Einstieg, Schichtenmodell, IP-Adressen, Filius-Workshop

* [:pptx: Präsentation WhatsApp-Beispiel](./0-whatsapp-beispiel.pptx)

- Präsentation: WhatsApp-Beispiel (was bereits angeschaut, was kommt dieses Jahr)

* [:pptx: Präsentation Schichtenmodell & IP-Adressen](./1-schichtenmodell-ip-adressen.pptx)

- Präsentation: Schichtenmodell, IP-Adresse bis Aufgaben zu Ping
- Kurzdemo Filius
- Die Schüler*innen beginnen selbständig den Filius-Workshop (2.1 - 2.4)
- Hausaufgabe: IP-Adresse des eigenen Notebooks zu Hause herausfinden und ein XLS-Liste eintragen

## 2. Doppellektion – Wichtige Begriffe, Filius-Workshop

* [:pptx: Präsentation wichtige Begriffe](./2-begriffe.pptx)

- Präsentation: Hausaufgabe besprechen
- Präsentation: Wichtige Begriffe
- Die Schüler*innen fahren selbständig mit dem Filius-Workshop weiter (2.5 - 2.6)
- Übung zu Routing
- Hausaufgabe: eigenes Netzwerk zu Hause zeichnen

## 3. Doppellektion – Protokolle, Filius-Workshop

* [:pptx: Präsentation Protokolle](./3-protokolle.pptx)

- Hausaufgabe besprechen und damit Begriffe (Switch, Router, Host, ...) repetieren
- Präsentation: Protokolle
- Die Schüler*innen fahren selbständig mit dem Filius-Workshop weiter (2.7 - 2.9)

## 4. Doppellektion – DNS, Filius-Workshop

* [:pptx: Präsentation DNS](./4-dns.pptx)

- Präsentation: DNS
- Die Schüler*innen fahren selbständig mit dem Filius-Workshop weiter (2.10 - 2.12)

## 5. Doppellektion – Fehlersuche, Filius-Workshop

* [:pptx: Präsentation Fehlersuche](./5-fehlersuche.pptx)

- Präsentation: Fehlersuche
- Filius-Übungen zu häufigen Problemen (2.13)

# Fakultative Themen

## Webtechnologien

- Kurze Einführung in HTML und HTTP.

## Bitübertragung

- Bitübertratung mit micro:bit


# alte Präsentation (all-in-one)

* [:pptx: Präsentation](./netzwerk-und-protokolle.pptx)
